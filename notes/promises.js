function makeRequest(msg){

    return new Promise( (resolve, reject) => {
        setTimeout(()=>{
            try{
                resolve('sukces! '+msg);
            }catch(e){
                reject(e);
            }
        },0)
    });

}

makeRequest('placki')
.then( result => result + '!!!' )
.then(
    result => console.log('result'),
    err => console.log(err)
)