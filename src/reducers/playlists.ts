import { Reducer, Action, ActionCreator } from "redux";
import { Playlist } from "../models/Playlist";

enum ActionTypes {
  LOAD = "[Playlists] LOAD",
  SELECT = "[Playlists] SELECT",
  EDIT = "[Playlists] EDIT",
  UPDATE = "[Playlists] UPDATE"
}
type PlaylistsState = {
  list: Playlist[];
  selected: Playlist["id"] | null;
  mode: "show" | "edit";
};
type Actions = LOAD | SELECT | EDIT | UPDATE;

const reducer: Reducer<PlaylistsState, Actions> = (
  state = {
    list: [],
    selected: null,
    mode: "show"
  },
  action
) => {
  switch (action.type) {
    case ActionTypes.LOAD:
      return { ...state, list: action.payload };

    case ActionTypes.SELECT:
      return { ...state, selected: action.payload };

    case ActionTypes.EDIT:
      return { ...state, mode: action.payload ? "edit" : "show" };

    case ActionTypes.UPDATE: {
      const draft = action.payload;
      return {
        ...state,
        list: state.list.map(p => (p.id === draft.id ? draft : p))
      };
    }

    default:
      return state;
  }
};

export default reducer;

interface LOAD extends Action<ActionTypes.LOAD> {
  payload: Playlist[];
}
interface SELECT extends Action<ActionTypes.SELECT> {
  payload: Playlist["id"];
}
interface EDIT extends Action<ActionTypes.EDIT> {
  payload: Playlist["id"];
}
interface UPDATE extends Action<ActionTypes.UPDATE> {
  payload: Playlist;
}

export const playlistsLoad: ActionCreator<LOAD> = payload => ({
  type: ActionTypes.LOAD,
  payload
});

export const PlaylistSelect: ActionCreator<SELECT> = payload => ({
  type: ActionTypes.SELECT,
  payload
});

export const edit: ActionCreator<EDIT> = (payload = true) => ({
  type: ActionTypes.EDIT,
  payload
});

export const update: ActionCreator<UPDATE> = payload => ({
  type: ActionTypes.UPDATE,
  payload
});

/* ============== */

export type State = {
  playlists: PlaylistsState;
};

export const playlistsSelectorFrom = (state: State): Playlist[] =>
  state.playlists.list;

export const selectedPlaylistSelectorFrom = (state: State): Playlist | null =>
  state.playlists.list.find(p => p.id == state.playlists.selected) || null;
