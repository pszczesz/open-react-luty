import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

import "./services";
import MusicSearchProvider from "./music/MusicSearchProvider";

// === REDUX ===
import { store } from "./store";
import { Provider } from "react-redux";
// (window as any).store = store;
// =============

// import { HashRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";

// window.React = React;
// window.ReactDOM = ReactDOM;

ReactDOM.render(
  <MusicSearchProvider>
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>
  </MusicSearchProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
