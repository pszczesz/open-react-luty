import PlaylistDetails from "../components/PlaylistDetails";
import { connect } from "react-redux";
import { PlaylistForm } from "../components/PlaylistForm";
import {
  State,
  selectedPlaylistSelectorFrom,
  edit,
  update
} from "../../reducers/playlists";
import { Playlist } from "../../models/Playlist";

export const SelectedPlaylistDetails = connect(
  (state: State) => ({
    playlist: selectedPlaylistSelectorFrom(state) || undefined
  }),
  dispatch => ({
    onEdit() {
      dispatch(edit(true));
    }
  })
)(PlaylistDetails);

export const SelectedPlaylistForm = connect(
  (state: State) => ({
    playlist: selectedPlaylistSelectorFrom(state)!
  }),
  dispatch => ({
    onCancel() {
      dispatch(edit(false));
    },
    onSave(draft: Playlist) {
      dispatch(update(draft));
      dispatch(edit(false));
    }
  })
)(PlaylistForm);
