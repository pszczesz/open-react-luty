import * as React from "react";
import { PlaylistsList } from "../containers/PlaylistsList";
import {
  SelectedPlaylistDetails,
  SelectedPlaylistForm
} from "../containers/SelectedPlaylist";
import { useStore, useSelector, useDispatch } from "react-redux";
import { State, PlaylistSelect } from '../../reducers/playlists';

const PlaylistsReactReduxView: React.FC = () => {

  const dispatch = useDispatch()
  
  const id = 123

  React.useEffect(()=>{
    dispatch(PlaylistSelect(id))
  },[id])

  const { mode, selected } = useSelector( // 
    (state: State) => state.playlists);

  return (
    <div>
      <div className="row">
        
        <div className="col">
          <PlaylistsList />
        </div>

        <div className="col">
          {selected && mode === "show" && <SelectedPlaylistDetails />}
          {selected && mode === "edit" && <SelectedPlaylistForm />}
        </div>

      </div>
    </div>
  );
};

export default PlaylistsReactReduxView;
