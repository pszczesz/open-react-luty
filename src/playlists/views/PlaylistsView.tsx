import React from "react";
import { ItemsList } from "../components/ItemsList";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { Playlist } from "../../models/Playlist";

type S = {
  selected: Playlist;
  playlists: Playlist[];
  mode: "edit" | "show";
  filter: string;
};

export class PlaylistView extends React.Component<{}, S> {
  state: S = {
    filter: "",
    selected: {
      id: 123,
      name: "React Hits!",
      color: "#ff00ff",
      favorite: true
    },
    playlists: [
      {
        id: 123,
        name: "React Hits!",
        color: "#ff00ff",
        favorite: true
      },
      {
        id: 234,
        name: "React Top20",
        color: "#00ffff",
        favorite: true
      },
      {
        id: 345,
        name: "Best of React",
        color: "#ffff00",
        favorite: false
      }
    ],
    mode: "show"
  };

  select = (selected: Playlist) => {
    this.setState({ selected: selected });
  };

  cancel = () => {
    this.setState({
      mode: "show"
    });
  };

  edit = () => {
    this.setState({
      mode: "edit"
    });
  };

  save = (draft: Playlist) => {
    console.log("save");

    this.setState(prevState => {
      const updatedList = prevState.playlists.map(p =>
        p.id === draft.id ? draft : p
      );

      // --- lub ---

      // const updatedList = [...prevState.playlists];
      // const index = updatedList.findIndex(p => p.id == draft.id);
      // if (index >= 0) {
      //   updatedList.splice(index, 1, draft);
      // }

      return {
        selected: draft,
        mode: "show",
        playlists: updatedList
      };
    });
  };

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            {/* <input
              type="text"
              onChange={e => this.setState({ filter: e.target.value })}
            /> */}

            <ItemsList
              // playlists={this.state.playlists.filter(p =>
              //   p.name.includes(this.state.filter)
              // )}
              playlists={this.state.playlists}
              selected={this.state.selected}
              onSelect={this.select}
            />
          </div>

          <div className="col">
            {this.state.mode === "show" ? (
              <PlaylistDetails
                onEdit={this.edit}
                playlist={this.state.selected}
              />
            ) : null}

            {this.state.mode === "edit" && (
              <PlaylistForm
                playlist={this.state.selected}
                onCancel={this.cancel}
                onSave={this.save}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}
