import React from "react";
import { ItemsList } from "../components/ItemsList";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { Playlist } from "../../models/Playlist";
import { store } from "../../store";
import { Unsubscribe } from "redux";

type S = {
  selected: Playlist | null;
  playlists: Playlist[];
  mode: "edit" | "show";
  filter: string;
};

export class PlaylistReduxView extends React.Component<{}, S> {
  state: S = {
    filter: "",
    selected: null,
    playlists: [],
    mode: "show"
  };

  componentDidMount() {
    const update = () => {
      const { list, selected } = store.getState().playlists;
      this.setState({
        playlists: list,
        selected: (selected && list.find(p => p.id === selected)) || null
      });
    };
    update();
    store.subscribe(() => {
      update();
    });
  }
  
  subscription!: Unsubscribe;

  componentWillUnmount() {
    this.subscription();
  }

  select = (selected: Playlist) => {
    this.setState({ selected: selected });
  };

  cancel = () => {
    this.setState({
      mode: "show"
    });
  };

  edit = () => {
    this.setState({
      mode: "edit"
    });
  };

  save = (draft: Playlist) => {
    this.setState(prevState => {
      const updatedList = prevState.playlists.map(p =>
        p.id === draft.id ? draft : p
      );

      return {
        selected: draft,
        mode: "show",
        playlists: updatedList
      };
    });
  };

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <ItemsList
              playlists={this.state.playlists}
              selected={this.state.selected}
              onSelect={this.select}
            />
          </div>

          <div className="col">
            {this.state.selected && this.state.mode === "show" ? (
              <PlaylistDetails
                onEdit={this.edit}
                playlist={this.state.selected}
              />
            ) : null}

            {this.state.selected && this.state.mode === "edit" && (
              <PlaylistForm
                playlist={this.state.selected}
                onCancel={this.cancel}
                onSave={this.save}
              />
            )}

            {!this.state.selected && <p>Please select playlist</p>}
          </div>
        </div>
      </div>
    );
  }
}
