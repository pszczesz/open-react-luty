import React from "react";
import { Playlist } from "../../models/Playlist";

type P = {
  playlist: Playlist;
  onCancel(): void;
  onSave(draft: Playlist): void;
};

type S = {
  draft: Playlist;
  error: string;
};

export class PlaylistForm extends React.PureComponent<P, S> {
  constructor(props: P) {
    super(props);
    console.log("constructor");
    this.state = {
      draft: this.props.playlist,
      error: ""
    };
  }

  // https://github.com/jaredpalmer/formik
  handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target;
    const nameRef = target.name;
    const value = target.type === "checkbox" ? target.checked : target.value;

    // (this.state.draft as any )[nameRef] = value

    // this.setState({})

    this.setState((prevState, prevProps) => {
      return {
        draft: {
          ...prevState.draft,
          [nameRef]: value
        }
      };
    });
  };

  inputRef = React.createRef<HTMLInputElement>();

  componentDidMount() {
    console.log("componentDidMount");

    if (this.inputRef.current) {
      this.inputRef.current.focus();
    }
  }

  render() {
    console.log("render");
    return (
      <>
        <div className="form-group">
          <label>Name:</label>
          <input
            ref={this.inputRef}
            type="text"
            onChange={this.handleInput}
            className="form-control"
            name="name"
            value={this.state.draft.name}
          />
          {170 - this.state.draft.name.length} / 170
          {this.state.error}
        </div>

        <div className="form-group">
          <label>Favorite:</label>
          <input
            type="checkbox"
            onChange={this.handleInput}
            checked={this.state.draft.favorite}
            name="favorite"
          />
        </div>

        <div className="form-group">
          <label>Color:</label>
          <input
            type="color"
            onChange={this.handleInput}
            value={this.state.draft.color}
            name="color"
          />
        </div>

        <input
          type="button"
          value="Cancel"
          className="btn btn-danger"
          onClick={this.props.onCancel}
        />

        <input
          type="button"
          value="Save"
          className="btn btn-success"
          onClick={() => this.props.onSave(this.state.draft)}
        />
      </>
    );
  }

  static defaultProps = {
    playlist: {
      id: 123,
      name: "React Hits!",
      color: "#ff00ff",
      favorite: true
    }
  };

  static getDerivedStateFromProps(nextProps: P, nextState: S): Partial<S> {
    console.log("getDerivedStateFromProps");

    return {
      draft:
        nextProps.playlist.id === nextState.draft.id
          ? nextState.draft
          : nextProps.playlist
    }; // nextState
  }

  // shouldComponentUpdate(newProps: P, newState: S) {
  //   console.log("shouldComponentUpdate");

  //   return (
  //     this.state.draft !== newState.draft ||
  //     this.props.playlist !== newProps.playlist
  //   );

  //   // return true;
  // }

  componentDidUpdate() {
    console.log("componentDidUpdate");
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
  }
}
