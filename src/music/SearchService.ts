import axios, { AxiosResponse, AxiosError } from "axios";
import { SearchResponse, Album, PagingObject } from "../models/Album";

export class SearchService {
  constructor(protected search_url: string) {}

  search(query = "batman") {
    const token = JSON.parse(sessionStorage.getItem("token") || "{}");
    if (!token.access_token) {
      return Promise.reject("Not logged in");
    }

    return (
      axios
        .get<SearchResponse>(this.search_url, {
          params: {
            type: "album",
            q: query
          },
          headers: {
            Authorization: "Bearer " + token.access_token
          }
        })
        // .then(results => {
        //   return new Promise<AxiosResponse<SearchResponse>>(resolve => {
        //     setTimeout(() => resolve(results), 2000);
        //   });
        // })
        .then(response => response.data.albums.items)
        .catch((err: AxiosError) => {
          if (err.response) {
            return Promise.reject(Error(err.response.data.error.message));
          }
          return Promise.reject(err);
        })
    );
  }
}

// fetch(this.search_url + "?q=" + query).then(resp =>
//   resp.json().then(data => console.log(data))
// );
