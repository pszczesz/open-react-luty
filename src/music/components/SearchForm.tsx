import * as React from "react";
import { useState } from "react";

type P = {
  onSearch(query: string): void;
  query?: string
};

export const SearchForm: React.FC<P> = ({ onSearch, query = '' }) => {

  const [localQuery, setQuery] = useState(query);

  React.useEffect(()=>{
    setQuery(query)
  },[query])

  return (
    <div>
      <div className="input-group mb-3">
        <input
          type="text"
          value={localQuery}
          className="form-control"
          placeholder="Search"
          onChange={e => setQuery(e.target.value)}
        />

        <div className="input-group-append">
          <button
            className="btn btn-outline-secondary"
            type="button"
            onClick={() => onSearch(localQuery)}
          >
            Search
          </button>
        </div>
      </div>
    </div>
  );
};

export default SearchForm;
