import * as React from "react";
import AlbumCard from "./AlbumCard";
import { Album } from "../../models/Album";

type P = {
  wyniki: Album[];
};

export const SearchResults: React.FC<P> = props => {
  return (
    <div>
      <div className="card-group">
        {props.wyniki.map(result => {
          return <AlbumCard album={result} key={result.id} />;
        })}
      </div>
    </div>
  );
};

export default SearchResults;
