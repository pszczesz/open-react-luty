import React, { FC } from "react";
// import logo from './logo.svg';
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { PlaylistView } from "./playlists/views/PlaylistsView";
import MusicSearchiew from "./music/views/MusicSearchView";
import MusicSearchiewCtx from "./music/views/MusicSearchiewCtx";
import { PlaylistReduxView } from "./playlists/views/PlaylistsReduxView";
import PlaylistsReactReduxView from "./playlists/views/PlaylistsReactReduxView";
import { Route, Switch, Redirect, NavLink, Link } from "react-router-dom";

const Layout: FC = props => (
  <div>
    <div className="row">
      <div className="col">{props.children}</div>
    </div>
  </div>
);

function App() {
  return (
    <div className="App">
      <nav className="navbar navbar-expand navbar-dark  bg-dark mb-3">
        <div className="container">
          <a className="navbar-brand" href="#/">
            MusicApp
          </a>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/search" activeClassName="placki active">
                  Search
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">
                  Playlists
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container">
        <Layout>
          <Switch>
            {/* <Route path="/" exact={true} component={PlaylistsReactReduxView} /> */}
            <Redirect path="/" exact={true} to="/playlists" />
            <Route path="/search" component={MusicSearchiew} />
            <Route path="/playlists" component={PlaylistsReactReduxView} />
            <Route path="**" render={() => <h4>Page not found!</h4>} />
          </Switch>
        </Layout>
      </div>
    </div>
  );
}

export default App;
