export interface Entity {
  // id: number | string | undefined;
  id: number;
  name: string;
}

export interface Track extends Entity {}

export interface Playlist extends Entity {
  favorite: boolean;
  color: string;
  tracks?: Track[];
  // tracks: Array<Track>
}
